

var ExternalService = (function() {

	var getJSON = function (url) {
	return new Promise(function (resolve, reject) {
		var xhr = new XMLHttpRequest();
		xhr.open('get', url, true);
		xhr.responseType = 'json';
		xhr.onload = function () {
			var status = xhr.status;
			if (status == 200) {
				resolve(xhr.response);
			} else {
				reject(status);
			}
		};
		xhr.send();
		});
	};

	return {
		getJsonData: getJSON
	}
})();

function PopulateTable(url) {
	ExternalService.getJsonData(url)
		.then((data) => {
			CreateAndFillTable(data);
		})
		.catch((error) => {
			console.log("error" + error);
		});
}

function CreateAndFillTable(data) {

	$('#myTable').remove();

	//add table element
	var e = $(document.createElement('table')).prop({
		id: 'myTable'
	}).appendTo(document.body);

	//create column headers
	$('#myTable').append(
		'<tr>' +
		$.map(data.columns, function (col, index) {
			return '<th>' + col + '</th>';

		}).join() + '</tr>');

	//insert rows in table
	$('#myTable').append(
		$.map(data.rows, function (row) {
			var e = $.map(row, function (elem) {
				return '<td>' + elem + '</td>';
			}).join();
			return '<tr>' + e + '</tr>';
		}).join());

	//add button for adding new row
	$('#addRows').remove();

	var e = $(document.createElement('input')).prop({
		id: 'addRows', type: 'file', onchange: 'AddRows()', accept: '.json'
	}).appendTo(document.body);
	document.getElementById("addRows").onchange = function() {AddRows()};
}

function AddRows() {
		ExternalService.getJsonData(document.getElementById('addRows').value)
		.then((data) => {
			$('#myTable').append( '<tr>' +
				$.map(data, function (elem) {
					return '<td>' + elem + '</td>';
			}).join() + '</tr>' );

		})
		.catch((error) => {
			console.log("error" + error);
		});
}
